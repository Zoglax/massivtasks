package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random rnd = new Random();

        //Task1
        /*int n=12;

        int[] mas = new int[n];

        for (int i=0; i<n; i++)
        {
            mas[i]=i+1;
            System.out.println(mas[i]);
        }*/
        //=============================

        //Task2

        /*int n=20;

        int[] mas = new int[n];

        for (int i=19; i>=0; i--)
        {
            mas[i]=i+1;
            System.out.println(mas[i]);
        }*/
        //=============================

        //Task3
        /*int n,index;

        System.out.println("Input massiv");
        n=scanner.nextInt();

        int[] mas = new int[n];

        for (int i=0; i<n; i++)
        {
            mas[i]=rnd.nextInt(100);
            System.out.print(mas[i]+" ");
        }

        System.out.println();

        System.out.println("Input index");
        index=scanner.nextInt();
        System.out.println(mas[index-1]);*/
        //=============================

        //Task4

        /*int n;

        System.out.println("Input massiv");
        n=scanner.nextInt();

        int[] mas = new int[n];

        System.out.println("Massiv before reverse order");
        for (int i=0; i<n; i++)
        {
            mas[i]=rnd.nextInt(100);
            System.out.print(mas[i]+" ");
        }
        System.out.println();
        System.out.println("Massiv after reverse order");
        for (int i=n-1; i>=0; i--)
        {
            System.out.print(mas[i]+" ");
        }*/
        //=============================

        //Task5

        int n;

        System.out.println("Input massiv");
        n=scanner.nextInt();

        int[] mas = new int[n];

        for (int i=0; i<n; i++)
        {
            mas[i]= (int) (Math.random() * 30) - 15;
            if (mas[i]>0)
            {
                System.out.println((i+1)+" element is : "+mas[i]);
            }
        }
    }
}
